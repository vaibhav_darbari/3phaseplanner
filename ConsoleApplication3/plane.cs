﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    //turning radius unused. turning restriction to be coded.
    class plane
    {
        double velocity; //in km/hr
        double time_step;  //in hrs
        public double turn_radius; // in Km
        public bool mission_complete;
        WayPoint current_pos;
        WPplan plan = new WPplan();

       
        public plane(string filepath,double velocity)
        {
            time_step = 0;
            plan.loadFile(filepath);
            current_pos = plan.getCurr();
            this.velocity = velocity;
        }
        public void travel()
        {
            if (!mission_complete)
            {
                if (!reached_nextWP())
                {
                    time_step = Simulation.time_interval;
                    double alt = plan.getCurr().alt + 8*time_step; // climbrate hard coded
                    current_pos = plan.plotWP(current_pos.lat, current_pos.lon, velocity * time_step);
                    current_pos.alt = alt;
                   // Console.WriteLine("current pos of plane:\n\tlat:" + current_pos.lat + "\tlon:" + current_pos.lon + "\talt:" + current_pos.alt + "\n");
                   // Console.WriteLine(velocity + "\n");

                }

                else
                {
                    time_step = 0;
                    mission_complete = plan.updateCurr();
                }
            }
        }

        double distcoord(double lat1, double lon1, double lat2, double lon2)
        {
            double earth = 6371;
            double dlat = torad(lat2 - lat1);
            double dlon = torad(lon2 - lon1);
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) + Math.Cos(torad(lat1)) * Math.Cos(torad(lat2)) * Math.Sin(dlon / 2) * Math.Sin(dlon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double dist = earth * c;
            return dist;
        }
        double torad(dynamic val)
        {
            return (val * Math.PI) / 180;
        }

        double todeg(dynamic val)
        {
            return (val * 180) / Math.PI;
        }

        bool reached_nextWP()  
        {
           
           if(distcoord(plan.getNext().lat,plan.getNext().lon,current_pos.lat,current_pos.lon)<0.010) //will work only if distannce covered in 0.1 secs by plan is less than 10m
            {
                
                return true;

            }
               
            else return false;
        }
        public void insert_DodgePoint(double bearing,double distance) //potential threat: dodgepoint queuing. drop old dodge points if updated!!
        {
            plan.insertWP(current_pos.lat, current_pos.lon, bearing, distance);
        }

         public WayPoint getCurrPos()
        {
            return current_pos;
        }  

        
    }
}
