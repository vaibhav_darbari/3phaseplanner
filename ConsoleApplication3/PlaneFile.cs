﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{

    class PlaneFile
    {
        public void write_plane(WayPoint Loc)
        {

            try
            {

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug\planejson.json"))
                {

                    file.WriteLine("{\"latitude\":" + Loc.lat + ",\"longitude\":" + Loc.lon + ",\"height\":" + Loc.alt + "}");
                    file.Close();
                }

            }

            catch
            {

            }


        }

    }
}
