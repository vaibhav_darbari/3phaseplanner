function [ idx,C ] = Cluster( X,K )
%K Means Clustering of sampled points
%  To identify voronoi centers and mark them as important waypoints.
%[idx,C]=kmeans(X,K);
[idx,C]=kmedoids(X,K);
figure;
hold on;
Center = zeros(3,K);
clrMap = jet(K);

for kk = 1:K

    plot3(X(idx==kk,1), X(idx==kk,2), X(idx==kk,3), 'LineStyle', 'none','Marker', 'o', 'color', clrMap(kk,:));
    xmean = mean(X(idx==kk,1));
    ymean = mean(X(idx==kk,2));
    zmean = mean(X(idx==kk,3));
    
    text(xmean, ymean, zmean, num2str(kk), 'color', 'k', 'FontWeight', 'Bold');
end

title('input samples');
xlabel('x(1)');
ylabel('x(2)');
zlabel('x(3)');
grid on;
view(-45, 25);
end

