﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class ObstMap
    {
        List<WayPoint> geofence=new List<WayPoint>();
        List<Cartesian> XYZcoord=new List<Cartesian>();
        public Grid Probability_map;
        public double Xmin { get; set; }
        public double Xmax { get; set; }
        public double Ymin { get; set; }
        public double Ymax { get; set; }
        public double Zmin { get; set; }
        public double Zmax { get; set; }

        Grid getProbMap()
        {
            return Probability_map;
        }
        public ObstMap()
        {
            Console.WriteLine("enter number of geofence points");
            int num = Convert.ToInt32(Console.ReadLine());

            for(int i=0;i< num;i++)
            {
                Console.WriteLine("enter lat lon of"+i.ToString()+" point");
                string input = Console.ReadLine();
                string[] split = input.Split(' ');
                WayPoint tmp=new WayPoint();
                tmp.lat = Convert.ToDouble(split[0]);
                tmp.lon = Convert.ToDouble(split[1]);
                tmp.alt = 100+i*50; 
                geofence.Add(tmp);

            }
            LLAtoECEF();
           // ECEFtoLLA();
            Probability_map = getRange(10);
           // initGrid(obst, 0.5, Probability_map);

        }
       
        List<Cartesian> LLAtoECEF()
        {
            
            for (int i = 0; i < geofence.Count; i++)
            {
                Cartesian tmp = new Cartesian();
                double cosLat = Math.Cos(torad(geofence[i].lat));
                double sinLat = Math.Sin(torad(geofence[i].lat));
                double cosLon = Math.Cos(torad(geofence[i].lon));
                double sinLon = Math.Sin(torad(geofence[i].lon));
                double radEarth= 6378137.0; //WARNING: Distance taken in meters!!!
                double f = 1.0 / 298.257224;
                double C = 1.0 / Math.Sqrt(cosLat * cosLat + (1 - f) * (1 - f) * sinLat * sinLat);
                double S = (1.0 - f) * (1.0 - f) * C;
                double h = geofence[i].alt;
                tmp.x = (radEarth * C + h) * cosLat * cosLon;  //h must also be in meters
                tmp.y = (radEarth * C + h) * cosLat * sinLon;
                tmp.z = (radEarth * S + h) * sinLat;
                Console.WriteLine(tmp.x + "\t" + tmp.y + "\t" + tmp.z);
                XYZcoord.Add(tmp);

            }
            return XYZcoord;

        }

        public Cartesian LLAtoECEF(WayPoint point)
        {
            Cartesian tmp = new Cartesian();
            double cosLat = Math.Cos(torad(point.lat));
            double sinLat = Math.Sin(torad(point.lat));
            double cosLon = Math.Cos(torad(point.lon));
            double sinLon = Math.Sin(torad(point.lon));
            double radEarth = 6378137.0; //WARNING: Distance taken in meters!!!
            double f = 1.0 / 298.257224;
            double C = 1.0 / Math.Sqrt(cosLat * cosLat + (1 - f) * (1 - f) * sinLat * sinLat);
            double S = (1.0 - f) * (1.0 - f) * C;
            double h = point.alt;
            tmp.x = (radEarth * C + h) * cosLat * cosLon;
            tmp.y = (radEarth * C + h) * cosLat * sinLon;
            tmp.z = (radEarth * S + h) * sinLat;

            return tmp;

        }

        public WayPoint ECEFtoLLA(Cartesian point)
        {
            // WGS84 ellipsoid constants
            double a = 6378137; // radius
            double e = 8.1819190842622e-2;  // eccentricity
            double asq = Math.Pow(a, 2);
            double esq = Math.Pow(e, 2);
            double b = Math.Sqrt(asq * (1 - esq));
            double bsq = Math.Pow(b, 2);
            double ep = Math.Sqrt((asq - bsq) / bsq);
            double p = Math.Sqrt(Math.Pow(point.x, 2) + Math.Pow(point.y, 2));
            double th = Math.Atan2(a * point.z, b * p);

            WayPoint tmp = new WayPoint();
            tmp.lon = Math.Atan2(point.y, point.x);
            tmp.lat= Math.Atan2((point.z + Math.Pow(ep, 2) * b * Math.Pow(Math.Sin(th), 3)), (p - esq * a * Math.Pow(Math.Cos(th), 3)));
            double N = a / (Math.Sqrt(1 - esq * Math.Pow(Math.Sin(tmp.lat), 2)));
            tmp.alt = p / Math.Cos(tmp.lat) - N;

            tmp.lat=todeg(tmp.lat);

            // mod lat to 0-2pi
            tmp.lon = tmp.lon % (2 * Math.PI);
            tmp.lon = todeg(tmp.lon);
            
            return tmp;


        }

        void ECEFtoLLA()
        {
            for(int i=0;i<XYZcoord.Count;i++)
            {
                WayPoint tmp = ECEFtoLLA(XYZcoord[i]);
                Console.WriteLine(tmp.lat.ToString() + "\t" + tmp.lon.ToString() + "\t" + tmp.alt.ToString());
            }
        }
        

        Grid getRange(double resol_dist)
        {
             Xmin= XYZcoord[0].x;
             Xmax= XYZcoord[0].x;
             Ymin= XYZcoord[0].y;
             Ymax= XYZcoord[0].y;
             Zmin = XYZcoord[0].z;
             Zmax = XYZcoord[0].z;

            for (int i=0;i<XYZcoord.Count;i++)
            {
                if (Math.Abs(XYZcoord[i].x) < Xmin)
                    Xmin = XYZcoord[i].x;
                else if (Math.Abs(XYZcoord[i].x) > Xmax)
                    Xmax = XYZcoord[i].x;

                if (Math.Abs(XYZcoord[i].y) < Ymin)
                    Ymin = XYZcoord[i].y;
                else if (Math.Abs(XYZcoord[i].y) > Ymax)
                    Ymax = XYZcoord[i].y;

                if (Math.Abs(XYZcoord[i].z) < Zmin)
                    Zmin = XYZcoord[i].z;
                else if (Math.Abs(XYZcoord[i].z) > Zmax)
                    Zmax = XYZcoord[i].z;


            }

            Grid tmp = new Grid();
            //change uav radius to something more appropriate. uav radius too big, obstacles with smaller radius may be missed
            //wrong axis assumption, allignment
            tmp.rows =Convert.ToInt32( Math.Floor(Math.Abs(Xmax - Xmin) / (2 *resol_dist))+1);   
            tmp.cols = Convert.ToInt32(Math.Floor(Math.Abs(Ymax - Ymin) / (2 * resol_dist))+1);
            tmp.height = Convert.ToInt32(Math.Floor(Math.Abs(Zmax - Zmin) / (2 * resol_dist)) + 1);
            tmp.initGrid();
            return tmp;
        }


       public void initGrid(List<Obstacle> obst, double resol_dist)
        {
            //TODO is inside polygon check and assign val 1 if outside. Status:DONE but not tested.
            double count1 = 0,count2=0,count3=0;
            //compute global score for each grid cell
            for (int i = 0; i < Probability_map.rows; i++)
            {
                for (int j = 0; j < Probability_map.cols; j++)
                {
                    for(int k=0;k< Probability_map.height; k++)
                    {
                        Cartesian point = new Cartesian();
                        point.x = Xmin + i * 2 * resol_dist;    //changed from  *resol_dist to 2*resol_dist 3 may 2016
                        point.y = Ymin + j * 2 * resol_dist;
                        point.z = Zmin + k * 2 * resol_dist;

                        // WayPoint tmp = XYZtoLatLon(point);
                        WayPoint tmp = ECEFtoLLA(point);
                        //Console.WriteLine(tmp.lat + " " + tmp.lon + " " + tmp.alt);
                        if (pointinPoly(geofence, tmp))
                        {
                            count1++;
                            bool first = false;
                            for (int l = 0; l < obst.Count; l++)
                            {
                                if (obst[l].getVelocity() == 0 && obst[l].isInside(tmp))
                                {
                                    Probability_map.probMap[i, j, k] = 1;
                                    count3++;
                                    break;

                                }

                                else if (obst[l].getVelocity() == 0)
                                {
                                    Probability_map.probMap[i, j, k] = globalScore(obst[l], point, Probability_map.probMap[i, j, k], first, obst.Count);
                                }

                            }
                        }

                        else
                        {
                            Probability_map.probMap[i, j, k] = 1; // outside geofence equals obstacle area therefore 1
                            count2++;
                        }
                    }
                   
                }
            }

            Console.WriteLine("count1 is:" + count1.ToString() + "\t count2 is" + count2.ToString()+"\t count3 is:"+count3.ToString());

            //compute local score ie mean value of all its neighbours for each cell and assign max(globalscore,localscore) as grid cell value
            int count_safe = 0;
            for (int i = 0; i < Probability_map.rows; i++)
            {
                for (int j = 0; j < Probability_map.cols; j++)
                {
                    for (int k = 0; k < Probability_map.height; k++)
                    {
                        Probability_map.probMap[i, j, k] = localScore(Probability_map, i, j, k);
                        if (Probability_map.probMap[i, j, k] <0.3)
                            count_safe++;
                    }
                }
            }
            Console.WriteLine("number of safe points( <0.3) is" + count_safe);
            //to remove direction bias
            for (int i = Probability_map.rows-1; i >=0; i--)
            {
                for (int j = Probability_map.cols-1; j >=0; j--)
                {
                    for (int k = Probability_map.height-1; k >=0; k--)
                    {
                        Probability_map.probMap[i, j, k] = localScore(Probability_map, i, j, k);

                    }
                }
            }
            ////test 1 beg:  res is that there is a diff abt 10m bw the distances calculated by the two methods for upto 500m
            //WayPoint pnt = new WayPoint();
            //pnt.lat = 28.753213;
            //pnt.lon = 77.113509;
            //pnt.alt = 100;
            //WayPoint t = obst[0].getCurr();
            
            //Cartesian test = LLAtoECEF(pnt);
            //Cartesian tst =LLAtoECEF(t);
            //Console.WriteLine(distcoord(pnt.lat, pnt.lon, t.lat, t.lon));
            //Console.WriteLine(Math.Sqrt(Math.Pow(test.x-tst.x,2)+ Math.Pow(test.y - tst.y, 2) + Math.Pow(test.z - tst.z, 2)));
            ////test 1 ends

        }

        double globalScore(Obstacle obst, Cartesian point,double curr_score,bool first,int count)
        {
            //way 1 dist in cartesian
            Cartesian obst_cent = LLAtoECEF(obst.getCurr());
            double val = Math.Sqrt(Math.Pow(obst_cent.x - point.x, 2) + Math.Pow(obst_cent.y - point.y, 2) + Math.Pow(obst_cent.z - point.z, 2));
            val = val - obst.ObstRadius; //changed val will effect global score
                                         //val *= 1000; //dist in m

           
            //possible improvement: instead of eucledian distance take no of cells as an indication of distance

            if (curr_score == 1)
                return 1;
            else
                curr_score += 1 / val;

            if(!first && curr_score!=0)
            {
                curr_score /= count;
                first = true;
            }
            return curr_score;
        }

        double localScore(Grid grid,int x, int y, int z )
        {
            if(grid.probMap[x,y,z]==1)
            {
                return 1;
            }

            int neighbourcount = 0;
            double value = 0;

            for(int i=-1;i<2;i++)
            {
                for(int j=-1;j<2;j++)
                {
                    for(int k=-1;k<2;k++)
                    {
                        int tx = x + i;
                        int ty = y + j;
                        int tz = z + k;

                        if (!(tx == x && ty == y && tz == z) && inRange(grid,tx, ty, tz))
                        {
                            value += grid.probMap[tx, ty, tz];
                            neighbourcount++;
                        }
                    }
                }
            }

            if (neighbourcount > 0)
                value /= neighbourcount;

            if (value > grid.probMap[x, y, z])
                return value;
            else
                return grid.probMap[x, y, z];

        }

        public double Dynamic_Score(List<Obstacle> obst,node wp)
        {
            double updated_score = wp.score;
            foreach(Obstacle mobile in obst)
            {
                if(mobile.getVelocity()!=0)
                {
                    globalScore(mobile, wp.Point,updated_score, true, obst.Count);
                }
            }
            return updated_score;
        }

        bool inRange(Grid grid,int x, int y, int z)
        {
            if (x >= 0 && x < grid.rows && y >= 0 && y < grid.cols && z >= 0 && z < grid.height)
                return true;
            else
                return false;
        }
        bool pointinPoly(List<WayPoint> LatLon, WayPoint point)
        {
            //Assumption: edges of polygon assumed to be straight lines, error due to curve neglected

            int i, j;
            bool c = false;
            for (i = 0, j = LatLon.Count - 1; i < LatLon.Count; j = i++)
            {
                if (((LatLon[i].lon > point.lon) != (LatLon[j].lon > point.lon)) && (point.lat < (LatLon[j].lat - LatLon[i].lat) * (point.lon - LatLon[i].lon) / (LatLon[j].lon - LatLon[i].lon) + LatLon[i].lat))
                    c = !c;
            }
            if (point.alt < 100 || point.alt>400) //for altitude limits for fence
                c = false;
            return c;
        }

        public void grid_csv()
        {
            string t = "";
            for(int i=0;i<Probability_map.rows;i++)
            {
                for(int j=0;j<Probability_map.cols;j++)
                {
                    for(int k=0;k<Probability_map.height;k++)
                    {
                        t += i + "," + j + "," + k + "," + Probability_map.probMap[i, j, k] + "\n";
                    }
                }
            }
            System.IO.File.WriteAllText(@".\prob_grid.csv", t);
        }
        public void print_grid()
        {
            string tmp = "";
            double val=Probability_map.probMap[0,0,0];
            WayPoint min=new WayPoint();

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug\prob_map.txt"))
            {
                for (int i = 0; i < Probability_map.rows; i++)
                {
                    // Console.Write("for x=" + i.ToString());
                    file.WriteLine("for x=" + i.ToString());

                    for (int j = 0; j < Probability_map.cols; j++)
                    {
                        for (int k = 0; k < Probability_map.height; k++)
                        {
                            //Console.Write(Probability_map.probMap[i, j, k].ToString() + "\t");
                            if (Probability_map.probMap[i, j, k] < val)
                            {
                                Cartesian point = new Cartesian();
                                point.x = Xmin + i * 10;
                                point.y = Ymin + j * 10;
                                point.z = Zmin + k * 10;
                                val = Probability_map.probMap[i, j, k];
                                min = ECEFtoLLA(point);
                            }

                             tmp += Probability_map.probMap[i, j, k].ToString() + "\t";
                           

                        }
                        //Console.Write("\n");
                        file.WriteLine(tmp);
                        tmp = "";
                       
                    }
                    file.WriteLine("\n\n");
                    //Console.Write("\n\n\n");
                }
                file.Close();
                
                Console.WriteLine("safest point is:" + min.lat + "\t" + min.lon + "\t" + min.alt);
            }
        }
        double distcoord(double lat1, double lon1, double lat2, double lon2)
        {
            double earth = 6371;
            double dlat = torad(lat2 - lat1);
            double dlon = torad(lon2 - lon1);
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) + Math.Cos(torad(lat1)) * Math.Cos(torad(lat2)) * Math.Sin(dlon / 2) * Math.Sin(dlon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double dist = earth * c;
            return dist;
        }
        double torad(dynamic val)
        {
            return (val * Math.PI) / 180;
        }

        double todeg(dynamic val)
        {
            return (val * 180) / Math.PI;
        }
    }

    struct Cartesian
    {
        public double x;
        public double y;
        public double z;
    }

    struct Grid
    {
        public int rows;
        public int cols;
        public int height;

        public double[, ,] probMap;
        public void initGrid()
        {
            probMap = new double[rows, cols,height];
        }
       

    }
}
