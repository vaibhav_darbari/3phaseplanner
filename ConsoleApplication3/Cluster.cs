﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Cluster
    {
       static MLApp.MLApp Matlab = new MLApp.MLApp();
        static double K = 8; // number of milestones i.e. voronoi partitions

       public static  List<Cartesian> Kmedoids(List<Cartesian> Samples)
        {
            double[,] Matrix = new double[Samples.Count, 3];
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug\Centers.csv"))
            {
                for (int i=0;i<Samples.Count;i++)
                {
                    Matrix[i, 0] = Samples[i].x;
                    Matrix[i, 1] = Samples[i].y;
                    Matrix[i, 2] = Samples[i].z;

                    file.WriteLine(Samples[i].x + "," + Samples[i].y + "," + Samples[i].z);
                   
                
                }
                file.Close();
            }

            List<Cartesian> tmp = new List<Cartesian>();
            object result = null;
            Matlab.Execute(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug");
            Matlab.Feval("Cluster", 2, out result, Matrix, K);
            object[] res = result as object[];
            //res[0] cluster index matrix for each sample
            //res[1] cluster centers matrix NxK
            object[] centers = res[1] as object[];
            foreach(var obj in centers)
            {
                Cartesian point = new Cartesian();
                object[] coordinates = obj as object[];
                point.x = Convert.ToDouble(coordinates[0]);
                point.y= Convert.ToDouble(coordinates[1]);
                point.z= Convert.ToDouble(coordinates[2]);

                tmp.Add(point);
            }
            return tmp;
        }

        public static List<IntList> Kmedoids(List<IntList> Samples)
        {
            double[,] Matrix = new double[Samples.Count, 3];
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug\Centers.csv"))
            {
                for (int i = 0; i < Samples.Count; i++)
                {
                    Matrix[i, 0] = Samples[i].x;
                    Matrix[i, 1] = Samples[i].y;
                    Matrix[i, 2] = Samples[i].z;

                    file.WriteLine(Samples[i].x + "," + Samples[i].y + "," + Samples[i].z);


                }
                file.Close();
            }

            List<IntList> tmp = new List<IntList>();
            object result = null;
            //Matlab.Execute(@"C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug");
            Matlab.Execute(@"cd 'C:\Users\vaibhav\Documents\Visual Studio 2015\Projects\ConsoleApplication3\ConsoleApplication3\bin\Debug'");
            Matlab.Feval("Cluster", 2, out result, Matrix, K);
            object[] res = result as object[];
            //res[0] cluster index matrix for each sample
            //res[1] cluster centers matrix NxK
            double [,] centers = res[1] as double[,];

            for(int i=0;i<centers.GetLength(0);i++)
            {
               
                    IntList point = new IntList();
                    point.x = Convert.ToInt32(centers[i,0]);
                    point.y = Convert.ToInt32(centers[i, 1]);
                    point.z = Convert.ToInt32(centers[i, 2]);
                    tmp.Add(point);

            }
            //foreach (var obj in centers)
            //{
            //    IntList point = new IntList();
            //    object[] coordinates = obj as object[];


            //    tmp.Add(point);
            //}
            return tmp;
        }
    }
}
