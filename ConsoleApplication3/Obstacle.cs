﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Obstacle
    {
        double velocity; //in km/hr
        double time_step;  //in hrs
        Int64 steps; //for resolution
        WayPoint current_pos;
        public double ObstRadius; //in meters
        List<WayPoint> obstacle = new List<WayPoint>();
        int curr_idx;

       public Obstacle(double cent_lat, double cent_lon, double alt, double obstRadius,double pathRadius, double velocity)
        {
            time_step = 0.1;
            this.velocity = velocity;
            velocity = velocity*5 / 18;
            if (velocity != 0)
                steps = Convert.ToInt64(2 * Math.PI * pathRadius / (velocity * time_step));
            else
                steps = 1;
            Console.WriteLine(pathRadius);
            makeCircle(cent_lat, cent_lon, alt,pathRadius, steps);
            //foreach(WayPoint ob in obstacle)
            //{
            //    Console.WriteLine(ob.lat + " " + ob.lon+"\n");
            //}
            ObstRadius = obstRadius;
            
            current_pos = obstacle[0];
            time_step = 0;
        }
        public void travel()
        { 
            //Console.WriteLine(current_pos.lat + " " + current_pos.lon + "\n");
                updateCurr();
            current_pos = obstacle[curr_idx];
            
        }

        public double getVelocity()
        {
            return velocity;
        }

        public WayPoint getCurr()
        {
            return current_pos;
        }
         void updateCurr()
        {
            if (curr_idx < obstacle.Count - 1)
            {
                curr_idx ++;
                //Console.WriteLine("obstacle now at obstaclepoint:" + curr_idx + "\n");
            }
            else
                curr_idx = 0; 
        }
        static double wrap(double brng)
        {
            if (brng > 360)
            { return brng - 360; }
            else if (brng < 0)
            { return 360 + brng; }
            else
            { return brng; }
        }

        public bool isInside(plane UAS)
        {
            if (distcoord(current_pos.lat, current_pos.lon, UAS.getCurrPos().lat, UAS.getCurrPos().lon) <= ObstRadius/1000)
            {
                return true;
            }
            else return false;
        }

        public bool isInside(WayPoint point)
        {
            if (distcoord(current_pos.lat, current_pos.lon, point.lat, point.lon) <= ObstRadius / 1000)
            {
                return true;
            }
            else return false;
        }
        double distcoord(double lat1, double lon1, double lat2, double lon2)
        {
            double earth = 6371;
            double dlat = torad(lat2 - lat1);
            double dlon = torad(lon2 - lon1);
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) + Math.Cos(torad(lat1)) * Math.Cos(torad(lat2)) * Math.Sin(dlon / 2) * Math.Sin(dlon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double dist = earth * c;
            return dist;
        }
        //static bearing for use in other modules: torad, todeg,wrap,bearing are all static
        static double torad(dynamic val)
        {
            return (val * Math.PI) / 180;
        }

        static double todeg(dynamic val)
        {
            return (val * 180) / Math.PI;
        }

        
        public static double bearing(dynamic lat1, dynamic lon1, dynamic lat2, dynamic lon2)
        {
            //double rad = 6371;
            double y = Math.Sin(torad(lon2) - torad(lon1)) * Math.Cos(torad(lat2));
            double x = Math.Cos(torad(lat1)) * Math.Sin(torad(lat2)) - Math.Sin(torad(lat1)) * Math.Cos(torad(lat2)) * Math.Cos(torad(lon2 - lon1));
            double brng = todeg(Math.Atan2(y, x));
            return wrap(brng);

        }
        public WayPoint updateLoc(double lat1, double lon1, double distance)
        {
            double brng = bearing(obstacle[curr_idx].lat, obstacle[curr_idx].lon, obstacle[curr_idx + 1].lat, obstacle[curr_idx].lon);
            double earth = 6371;
            double flat = Math.Asin(Math.Sin(torad(lat1)) * Math.Cos(distance / earth) + Math.Cos(torad(lat1)) * Math.Sin(distance / earth) * Math.Cos(torad(brng)));
            double flon = torad(lon1) + Math.Atan2(Math.Sin(torad(brng)) * Math.Sin(distance / earth) * Math.Cos(torad(lat1)), Math.Cos(distance / earth) - Math.Sin(torad(lat1)) * Math.Sin(flat));
            flat = todeg(flat);
            flon = todeg(flon);
            //Console.WriteLine("shift to lat " + flat + "lon " + flon);
            WayPoint result = new WayPoint();
            result.lat = flat;
            result.lon = flon;

            
            return result;

        }
        bool reached_nextLoc() //valid only for line segments!
        {
            // if (current_pos.lat < obstacle[curr_idx].lat && current_pos.lon < obstacle[curr_idx].lon || current_pos.lat > obstacle[curr_idx].lat && current_pos.lon > obstacle[curr_idx].lon)
            if (curr_idx < obstacle.Count - 1)
            {
                if (distcoord(obstacle[curr_idx + 1].lat, obstacle[curr_idx + 1].lon, current_pos.lat, current_pos.lon) < 0.0001)
                    return true;
                else return false;
            }
            else return true;
        }
         void makeCircle(double centerLat, double centerLng, double alt, double radius,Int64 steps) 
        {
            var pi2 = Math.PI * 2;
            radius = radius / 111120;
            for (int i = 0; i < steps; i++)
            {
                //problem here
                double lat = centerLat + radius * Math.Cos((double)i / steps * pi2);
                double lng = centerLng + radius * Math.Sin((double)i / steps * pi2);
                double height = alt - (alt * (0.5 - i / steps) / 2);
                WayPoint tmp = new WayPoint();
                tmp.lat = lat;
                tmp.lon = lng;
                obstacle.Add(tmp);
            }
        }
    }

   
}
