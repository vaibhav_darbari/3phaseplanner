﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleApplication3
{
    class Simulation
    {
        //28.750428 77.116910 100.000000 30 60 35
        List<Obstacle> obst = new List<Obstacle>();
        List<plane> uav = new List<plane>();
        ObstMap Probability_Map;
        Graph PRM;
        bool continue_sim;
        bool collision;
        bool mission_complete;
        public static double time_interval = 0.00002778; //default value is 0.1 sec
        System.Timers.Timer timer = new System.Timers.Timer(100);
        PlaneFile pFile = new PlaneFile();
        DynamicObstFile dFile = new DynamicObstFile();
        
        static void Main(string[] args)
        {
            Simulation Sim = new Simulation();
            Console.WriteLine("enter number of obstacles\n");
           
            string a = Console.ReadLine();
            
            int num = Convert.ToInt32(a);
            
            for (int i=0;i< num;i++)
            { 
                Console.WriteLine("enter centerLat centerLon centerAlt ObstRadius PathRadius  Velocity seperated by space\n");
                string line = Console.ReadLine();
                string[] param = line.Split(' ');
               
                double lat = Convert.ToDouble(param[0]);
                double lon= Convert.ToDouble(param[1]);
                double alt= Convert.ToDouble(param[2]);
                double radius= Convert.ToDouble(param[3]);
                double pathRadius = Convert.ToDouble(param[4]);
                //int steps = Convert.ToInt32(param[4]);
                double velocity= Convert.ToDouble(param[5]);
                Obstacle tmp = new Obstacle(lat,lon,alt,radius,pathRadius,velocity);

                
                Sim.obst.Add(tmp);
                
            }
            //Obstacle a = new Obstacle();

            //Initiate obstacle map to represent C-space
            Sim.Probability_Map = new ObstMap();
            Sim.Probability_Map.initGrid(Sim.obst, 10); //TODO resol_dist as a constant param across all files.
            Sim.Probability_Map.print_grid();
            Sim.Probability_Map.grid_csv();

            //initiate graph for building PRM
            Sim.PRM = new Graph(Sim.Probability_Map);
            Sim.PRM.getCenters();
            Sim.PRM.displayCenters();
            Sim.PRM.initGraph(120); //40
           

            Sim.Begin();
            Console.Read();
        }

      public Simulation()
        {

            timer.Elapsed += One_deciSecond_Loop;
            Console.WriteLine("enter Plane velocity");
            double velocity = Convert.ToDouble(Console.ReadLine());
            plane tmpPlane = new plane("./gen_WP.txt",velocity);
            uav.Add(tmpPlane);
            continue_sim = true;

            
        }

        private void One_deciSecond_Loop(object sender, ElapsedEventArgs e)
        {
            if (continue_sim)
            {
                mission_complete = true;
                foreach (plane UAS in uav)
                {
                    UAS.travel();
                    mission_complete = mission_complete && UAS.mission_complete;
                    pFile.write_plane(UAS.getCurrPos());
                   
                }
                foreach (Obstacle sphere in obst)
                {
                    sphere.travel();
                    dFile.write_Obst(sphere.getCurr(), sphere.ObstRadius);
                }

                if (detectCollision())
                {
                    continue_sim = false;
                }

                if(mission_complete)
                {
                    Console.WriteLine("Mission was completed Successfuly!\n");
                    continue_sim = false;   
                }

            }
            else
                End();
        }

        public void Begin()
        {
            timer.Start();
            Console.WriteLine("Simulation Started!\n");
        }

        void End()
        {
            timer.Stop();
            Console.WriteLine("Simulation Ended!");
        }
        
        bool detectCollision()
        {
            foreach(Obstacle sphere in obst)
            {
                foreach(plane UAS in uav)
                {
                    if(sphere.isInside(UAS))
                    {
                        collision = true;
                        WayPoint colObst = sphere.getCurr();
                        WayPoint colUAS = UAS.getCurrPos();
                        string res = "collision data\n";
                       
                        res += "obstacle involved:\n lat:" + colObst.lat + "\tlon:" + colObst.lon + "\talt:" + colObst.alt + "\tradius:" + sphere.ObstRadius+"\n";
                        res += "UAS involved:\n lat:" + colUAS.lat + "\tlon:" + colUAS.lon + "\talt:" + colUAS.alt;
                        Console.WriteLine(res);
                        return true;

                    }
                }
            }
            return false;

        }

       
    }

    struct WayPoint
    {
        public double lat;
        public double lon;
        public double alt;
        public bool dodge;
    }
}
