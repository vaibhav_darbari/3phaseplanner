﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    
    class WPplan
    {
        List<WayPoint> path = new List<WayPoint>();
        string[] wp;
        int current_wp;

        public WPplan()
        {
            current_wp = 0;
        }

        public void loadFile(string filepath)
        {
            using (StreamReader reader = new StreamReader(filepath))
            {
                string line = reader.ReadLine();
                //string line1 = reader.ReadLine();

                while (line != null)
                {

                    line = reader.ReadLine();
                    if (line == null)
                        break;
                    wp = line.Split('\t');
                    WayPoint item = new WayPoint();

                    item.lat = Convert.ToDouble(wp[8]);
                    item.lon = Convert.ToDouble(wp[9]);
                    item.alt = Convert.ToDouble(wp[10]);
                    Console.WriteLine("altitude is" + item.alt + "\n");
                    item.dodge = false;
                    path.Add(item);
                }

            }
        }
        public WayPoint getNext()
        {
            if (current_wp < path.Count - 1)
                return path[current_wp+1];
            else return  path[current_wp]; //prob
        }
        public WayPoint getCurr()
        {
            return path[current_wp];
        }
        public bool updateCurr()
        {
            if (current_wp < path.Count - 1)
            {
                
                current_wp = current_wp + 1;
                Console.WriteLine("plane now at wp:" + current_wp + "\n");
            }

            else
            {
                Console.WriteLine("plane completed mission safely! Congratulations\n");
                return true;
            }
            return false;
        }
        public WayPoint insertWP(double lat1, double lon1, double brng,double distance)
        {
           
            double earth = 6371;
            double flat = Math.Asin(Math.Sin(torad(lat1)) * Math.Cos(distance / earth) + Math.Cos(torad(lat1)) * Math.Sin(distance / earth) * Math.Cos(torad(brng)));
            double flon = torad(lon1) + Math.Atan2(Math.Sin(torad(brng)) * Math.Sin(distance / earth) * Math.Cos(torad(lat1)), Math.Cos(distance / earth) - Math.Sin(torad(lat1)) * Math.Sin(flat));
            flat = todeg(flat);
            flon = todeg(flon);
            //Console.WriteLine("shift to lat " + flat + "lon " + flon);
            WayPoint result = new WayPoint();
            result.lat = flat;
            result.lon = flon;
            result.alt = path[current_wp].alt;
            result.dodge = true;

            if (path[current_wp + 1].dodge == true)  //prevents dodgepoint queuing
                path.RemoveAt(current_wp + 1);

            path.Insert(current_wp + 1, result);
            return result;

        }
        public WayPoint plotWP(double lat1, double lon1,  double distance)
        {
            double brng = bearing(path[current_wp].lat, path[current_wp].lon, path[current_wp + 1].lat, path[current_wp+1].lon);
            double earth = 6371;
            double flat = Math.Asin(Math.Sin(torad(lat1)) * Math.Cos(distance / earth) + Math.Cos(torad(lat1)) * Math.Sin(distance / earth) * Math.Cos(torad(brng)));
            double flon = torad(lon1) + Math.Atan2(Math.Sin(torad(brng)) * Math.Sin(distance / earth) * Math.Cos(torad(lat1)), Math.Cos(distance / earth) - Math.Sin(torad(lat1)) * Math.Sin(flat));
            flat = todeg(flat);
            flon = todeg(flon);
            //Console.WriteLine("shift to lat " + flat + "lon " + flon);
            WayPoint result = new WayPoint();
            result.lat= flat;
            result.lon = flon;

            //path.Insert(current_wp + 1, result);
            return result;

        }

        double torad(dynamic val)
        {
            return (val * Math.PI) / 180;
        }

        double todeg(dynamic val)
        {
            return (val * 180) / Math.PI;
        }


        double bearing(dynamic lat1, dynamic lon1, dynamic lat2, dynamic lon2)
        {
            //double rad = 6371;
            double y = Math.Sin(torad(lon2) - torad(lon1)) * Math.Cos(torad(lat2));
            double x = Math.Cos(torad(lat1)) * Math.Sin(torad(lat2)) - Math.Sin(torad(lat1)) * Math.Cos(torad(lat2)) * Math.Cos(torad(lon2 - lon1));
            double brng = todeg(Math.Atan2(y, x));
            return wrap(brng);

        }


       double wrap(double brng)
        {
            if (brng > 360)
            { return brng - 360; }
            else if (brng < 0)
            { return 360 + brng; }
            else
            { return brng; }
        }
    }
}
