﻿/*Copyright(C) 2016  Vaibhav Darbari

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/> */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Graph
    {
        List<Cartesian> SafeSamples = new List<Cartesian>();
        List<Cartesian> Centers=new List<Cartesian>();
        List<IntList> Index=new List<IntList>();
        List<IntList> clusterList;
        List<node> PRM=new List<node>();
        List<node> Milestones = new List<node>();
        Random rnd = new Random();
        ObstMap Map;
        const int num_sample = 6000;
        const int score_wt = 10;
        const double min_turn_ang = 120;   //90
        const double max_turn_ang = 210;   //270
        const double climb_rate = 8;
        const double velocity = 45;

        //for depth bound node expansion illustration
        string motion_tmp = "";
        string motion_primitives_node = "";
        int path_count = 0;
        public Graph(ObstMap Map)
        {
            this.Map = Map;
            getSamples();
        }

        public void initGraph(int k)
        {
            //tmp list wp
            List<WayPoint> tmplist = new List<WayPoint>();
            for (int i = 0; i < Index.Count; i++)
            {
                //Console.WriteLine("Id is"+i+"\tX is" + clusterList[i].x + "\t Y is" + clusterList[i].y + " \tZ is" + clusterList[i].z);
                Cartesian point = new Cartesian();
                point.x = Map.Xmin + Index[i].x * 20; // resol_dist
                point.y = Map.Ymin + Index[i].y * 20;
                point.z = Map.Zmin + Index[i].z * 20;

                IntList idx = new IntList();
                idx.x = Index[i].x;
                idx.y = Index[i].y;
                idx.z = Index[i].z;

                node tmp = new node(0);
                tmp.Point = point;
                tmp.index = idx;
                tmp.score = Map.Probability_map.probMap[Index[i].x, Index[i].y, Index[i].z]*score_wt;
                PRM.Add(tmp);
                Console.WriteLine("nodes added to PRM");

            }
            //for(int i=10;i<15;i++)
            //{
            //    for(int j=0;j<5; j++)
            //    {
            //        for(int z=0;z<5;z++)
            //        {
            //            Cartesian point2 = new Cartesian();
            //            point2.x = Map.Xmin + i * 20; // resol_dist
            //            point2.y = Map.Ymin + j * 20;
            //            point2.z = Map.Zmin + z * 20;

            //            WayPoint tmp1 = Map.ECEFtoLLA(point2);
            //            tmplist.Add(tmp1);
            //        }
            //    }
            //}
            ////problem identified : majority points outside grid
            //writefile(tmplist);
            for(int i=0;i<PRM.Count;i++)
            {

                kNN(PRM[i], k);
                Console.WriteLine("k nearest noded connected for\t"+i);
            }
            removeUnsafe(); //removes unsafe edges
            Console.WriteLine("unsafe edges removed");
            //displayGraph();          //only for display takes lot of time

            //example: replace after testing
            WayPoint pnt = new WayPoint();
            pnt.lat = 28.753213;
            pnt.lon = 77.113509;
            pnt.alt = 100;
            
            SetOrder(Map.LLAtoECEF(pnt));

            //List<node> res=AStrSearch(PRM[1], PRM[2]);
            //for(int i=0;i<res.Count;i++)
            //{
            //    Console.WriteLine(res[i].Point.x + " " + res[i].Point.y + " " + res[i].Point.z);
            //}
            //end of example
            List<WayPoint> WP = new List<WayPoint>();
            for (int i=0;i<Milestones.Count-1;i++)
            {
                node src=null, dst=null;
                for (int j=0;j<PRM.Count;j++)
                {
                    
                    if(PRM[j].Point.x==Milestones[i].Point.x && PRM[j].Point.y == Milestones[i].Point.y && PRM[j].Point.z== Milestones[i].Point.z)
                    {
                        src = PRM[j];
                    }
                    else if(PRM[j].Point.x == Milestones[i+1].Point.x && PRM[j].Point.y == Milestones[i+1].Point.y && PRM[j].Point.z == Milestones[i+1].Point.z)
                    {
                        dst = PRM[j];
                    }
                   
                }

               //prob: j skip as src or dst = null. Effect:cases where j skips leads to sharp turns!!
               if(src!=null && dst!=null)
                {
                    //if (WP.Count > 0) //enforce dynamic constraints between milestone segments
                    //{
                    //    node connecting_node = new node(0);
                    //    connecting_node.Point = Map.LLAtoECEF(WP[WP.Count - 1]);
                    //    src.parent = connecting_node;
                    //}
                    List<node> res = AStrSearch(src, dst);

                    List<WayPoint> independent_path=new List<WayPoint>();
                    for (int z = 0; z < res.Count; z++)
                    {
                        WayPoint t = Map.ECEFtoLLA(res[z].Point);
                        //Console.WriteLine(res[z].Point.x + " " + res[z].Point.y + " " + res[z].Point.z);
                        Console.WriteLine(t.lat + " " + t.lon + " " + t.alt);
                        WP.Add(t);
                        independent_path.Add(t);
                    }
                    writefile(independent_path, "path_segment" + path_count + ".txt");
                    path_count++;
                }
                else if (dst == null)// && i != Milestones.Count - 2)   //experimental fix : acute turns still exist
                {
                    Milestones.RemoveAt(i + 1);
                    //Milestones[i+1].parent
                    Console.WriteLine("destination milestone vanished!");
                    i--;
                }

            }
            Console.WriteLine("writing file");
            writefile(WP,"gen_WP.txt");
            // generate a sample depth bound motion primitives tree. 
            //int max_prm_node = 0, max_prm_idx = 0;
            //for (int z = 0; z < PRM.Count; z++)
            //    if (PRM[z].neighbours.Count > max_prm_node)
            //    {
            //        max_prm_node = PRM[z].neighbours.Count;
            //        max_prm_idx = z;
            //    }
            node motion_primtive=DepthBound_BFS(PRM[10], 0);
            Disp_Motion_Primitives(motion_primtive,0);

        }
        void kNN(node current, int k) //potential prob: independent strongly connected components may form leading to forests instead of a graph!
        {
            if (k > current.neighbours.Count)
            {
                List<node> neighbour = new List<node>();
                List<nodeDist> distances = new List<nodeDist>();
                for (int i = 0; i < PRM.Count; i++)
                {
                    nodeDist tmp = new nodeDist();
                    tmp.dist = Math.Sqrt(Math.Pow(current.Point.x - PRM[i].Point.x, 2) + Math.Pow(current.Point.y - PRM[i].Point.y, 2) + Math.Pow(current.Point.z - PRM[i].Point.z, 2));
                    tmp.Node = PRM[i];
                    if (PRM[i].GetHashCode() != current.GetHashCode())
                    {
                        distances.Add(tmp);
                    }
                }
                distances.Sort(delegate (nodeDist c1, nodeDist c2) { return c1.dist.CompareTo(c2.dist); });
                int diff = k - current.neighbours.Count;
                int j = 0;
                for (int i = 0; i < diff;)
                {
                    if (notIn(current.neighbours, distances[j].Node)) // notin not working, issue solved probably recheck if repetition persists
                    {
                        current.neighbours.Add(distances[j].Node);
                        distances[j].Node.neighbours.Add(current);
                        i++;
                    }
                    j++;
                }
            }
        }

        List<node> AStrSearch(node src,node dst)
        {
            List<node> Open = new List<node>();
            double f = src.pcost + src.score; //normalize pcost and score on same scale for equal weightage : status done just vary score_wt
            int idx = 0;
            node Bestnode=src;
            Open.Add(src);
            List<node> Closed = new List<node>();
            do
            {
                if (Open.Count == 0)
                {
                    Console.WriteLine("AStar: no BestNode found, Path not possible");
                    break;
                }

                f= Open[0].pcost + Open[0].score; //changed to reset f after every cycle
                for (int i=0;i<Open.Count;i++)   // f to be refreshed after every bestdnode analyzed
                {
                    double ftmp = Open[i].pcost + Open[i].score;
                    if(ftmp<=f)
                    {
                        Bestnode = Open[i];
                        idx = i;
                        f = ftmp;
                    }
                }

                Open.RemoveAt(idx);
                Closed.Add(Bestnode);

                if(Bestnode.GetHashCode()==dst.GetHashCode())
                {
                    Bestnode.visited = 1;
                    //test 3 could create prob
                    dst.parent = Bestnode.parent;
                    Console.WriteLine(" executed for path creation!");
                }

                else
                {
                    foreach(node succ in Bestnode.neighbours)
                    {
                        bool in_open = false;
                        bool in_closed = false;
                        for(int i=0;i<Open.Count;i++) // replace with node.find for better performance!!
                        {
                            if(succ.GetHashCode()==Open[i].GetHashCode() && Open[i].pcost<=Bestnode.pcost + eucl_dist(Bestnode, Open[i])) //replace 1 with eucledian dist: status done
                            {
                                in_open = true;
                                break;
                            }
                        }
                        for(int i=0;i<Closed.Count;i++)
                        {
                            if (succ.GetHashCode() == Closed[i].GetHashCode())
                            {
                                in_closed = true;
                                //if (succ.pcost>Bestnode.pcost+1) //replace 1 with eucledian dist
                                if(succ.pcost>Bestnode.pcost + eucl_dist(Bestnode, succ))
                                {
                                    WayPoint wp0 = Map.ECEFtoLLA(Bestnode.parent.Point);
                                    WayPoint wp1 = Map.ECEFtoLLA(Bestnode.Point);
                                    WayPoint wp2 = Map.ECEFtoLLA(succ.Point);
                                    double ang1 = Obstacle.bearing(wp1.lat, wp1.lon, wp0.lat, wp0.lon);
                                    double ang2 = Obstacle.bearing(wp1.lat, wp1.lon, wp2.lat, wp2.lon);
                                    double res = Math.Abs(ang1 - ang2);
                                    double dist = eucl_dist(Bestnode, succ);
                                    double time = dist / velocity;
                                    double time2 = Math.Abs(wp1.alt - wp2.alt) / climb_rate;
                                    if (res > min_turn_ang && res < max_turn_ang && time2 < time)   //dist factor removed leading to close points
                                    {
                                        succ.parent = Bestnode;  //TODO:check dynamic constraints before declaring parent
                                                                 //succ.pcost = Bestnode.pcost + 1; //replace 1 with eucledian dist
                                        succ.pcost = Bestnode.pcost + eucl_dist(Bestnode, succ);
                                        DFT(succ);

                                    }

                                   
                                }
                                break;
                            }
                        }
                        if(!in_open && !in_closed)
                        {
                            //succ.parent = Bestnode;
                            //succ.pcost = Bestnode.pcost + 1;  // use normalised eucledian distance instead of 1 for better estimation
                            succ.pcost = Bestnode.pcost + eucl_dist(Bestnode,succ);
                            //check dynamic constraints
                            if (Bestnode.parent != null)
                            {
                                WayPoint wp0 = Map.ECEFtoLLA(Bestnode.parent.Point);
                                WayPoint wp1 = Map.ECEFtoLLA(Bestnode.Point);
                                WayPoint wp2 = Map.ECEFtoLLA(succ.Point);
                                double ang1 = Obstacle.bearing(wp1.lat, wp1.lon, wp0.lat, wp0.lon);
                                double ang2 = Obstacle.bearing(wp1.lat, wp1.lon, wp2.lat, wp2.lon);
                                double res = Math.Abs(ang1 - ang2);
                                double dist = eucl_dist(Bestnode, succ);
                                double time = dist / velocity;
                                double time2 = Math.Abs(wp1.alt - wp2.alt) / climb_rate;
                                if (res > min_turn_ang && res < max_turn_ang && time2 < time)   //dist factor removed leading to close points
                                {
                                    Open.Add(succ);
                                    succ.parent = Bestnode;
                                }
                                // potential prob: fix attempt
                            }
                            else
                            {
                                Open.Add(succ);
                                succ.parent = Bestnode; // potential prob: fix attempt
                            }
                        }
                        
                        
                    }
                }

            }
            while (Bestnode.visited != 1);

            List<node> path = new List<node>();
            if(Bestnode.visited==1)
            {
                ConstructPath(Bestnode,path,src);
                //test 4 could create prob
                Bestnode.visited = 0;
            }
            for (int z = 0; z < path.Count - 1; z++)   //remove redundant close points
            {
                if (eucl_dist(path[z], path[z + 1]) < 15)
                {
                    path.RemoveAt(z + 1);
                }

            }
            return path;
        }

        void DFT(node src)
        {
            foreach(node subsucc in src.neighbours)
            {
                if(subsucc.parent.GetHashCode()==src.GetHashCode())
                {
                    // subsucc.pcost = src.pcost + 1; //replace with eucl dist
                    subsucc.pcost = src.pcost + eucl_dist(src, subsucc);
                    DFT(subsucc);
                }
            }
        }

        node DepthBound_BFS(node src, int depth)
        {
            if (depth > 3)
                return new node(0);
            node example = new node(0);
            example.Point = src.Point;
            
            foreach(node succ in src.neighbours)
            {
                if(src.parent!=null)
                            {
                                WayPoint wp0 = Map.ECEFtoLLA(src.parent.Point);
                                WayPoint wp1 = Map.ECEFtoLLA(src.Point);
                                WayPoint wp2 = Map.ECEFtoLLA(succ.Point);
                                double ang1 = Obstacle.bearing(wp1.lat, wp1.lon, wp0.lat, wp0.lon);
                                double ang2 = Obstacle.bearing(wp1.lat, wp1.lon, wp2.lat, wp2.lon);
                                double res = Math.Abs(ang1 - ang2);
                                double dist = eucl_dist(src, succ);
                                double time = dist / velocity;
                                double time2 = Math.Abs(wp1.alt - wp2.alt) / climb_rate;
                                if (res > min_turn_ang && res < max_turn_ang && time2 < time)   //dist factor removed leading to close points
                                {
                                    succ.parent = example;
                                    node mod_neighbour = DepthBound_BFS(succ, depth + 1);
                                    succ.neighbours = mod_neighbour.neighbours;
                                    example.neighbours.Add(succ);
                                }
                            }
                else
                {
                    succ.parent = example;
                    node mod_neighbour = DepthBound_BFS(succ, depth + 1);
                    succ.neighbours = mod_neighbour.neighbours;
                    example.neighbours.Add(succ);
                }

            }

            return example;
        }

        void Disp_Motion_Primitives(node src,int depth)
        {
            string tmp = "";
            if(depth<=3 && src.neighbours.Count>0)
            {
                foreach(node succ in src.neighbours)
                {
                    Disp_Motion_Primitives(succ,depth + 1);
                    motion_tmp += src.Point.x.ToString() + "\t" + src.Point.y.ToString() + "\t" + src.Point.z.ToString()+"\n";
                    motion_tmp += succ.Point.x.ToString() + "\t" + succ.Point.y.ToString() + "\t" + succ.Point.z.ToString() + "\n";
                    motion_tmp += "\n\n";
                }
            }

            motion_primitives_node += src.Point.x.ToString() + "\t" + src.Point.y.ToString() + "\t" + src.Point.z.ToString() + "\t" + depth.ToString() + "\n";
            if (depth == 0)
            {
                System.IO.File.WriteAllText("./motion_primitives.txt", motion_tmp);
                System.IO.File.WriteAllText("./motion_primitives_node.txt", motion_primitives_node);
            }
           // return tmp;
        }
        //void ConstructPath(node src, List<node> path)
        void ConstructPath(node src, List<node> path, node parent)
        {
            //if (src.parent != null) // assumption that non set node is null
            if (src.parent != parent) 
            {
                ConstructPath(src.parent, path,parent);
                path.Add(src);
            }
            else
                path.Add(src); //verify validity
        }
        double eucl_dist(node src,node dst)
        {
            double dist = Math.Sqrt(Math.Pow(src.Point.x - dst.Point.x, 2) + Math.Pow(src.Point.y - dst.Point.y, 2) + Math.Pow(src.Point.z - dst.Point.z, 2));
            return dist;
        }
        void SetOrder(Cartesian src)
        {
            node start = new node(0);
            start.Point = src;
            Milestones.Add(start);

            for (int i=0;i<Centers.Count;i++)
            {
                node mil = new node(0);
                mil.Point = Centers[i];
               
                Milestones.Add(mil);
            }
           
            int j = 0;
            node tmp=Milestones[0];
            
            while(j<=Milestones.Count)
            {
                tmp = getNextMilestone(tmp,j+1);
                //tmp.score = j+1;
                j++;
            }
            Milestones.Sort(delegate (node c1, node c2) { return c1.score.CompareTo(c2.score); });
            
        }
        node getNextMilestone(node src,int pos)
        {
            int j = 0,idx=0;
            double nearest = 99999999999; //INF
            src.visited = 1;
            //node tmp;
            for (int i=0;i<Milestones.Count;i++)
            {
                //nodeDist tmp = new nodeDist();
                
                double dist = Math.Sqrt(Math.Pow((Milestones[i].Point.x - src.Point.x), 2) + Math.Pow((Milestones[i].Point.y - src.Point.y), 2) + Math.Pow((Milestones[i].Point.z - src.Point.z), 2));
                
                if (dist != 0 && dist < nearest && Milestones[i].visited!=1)
                {
                   // tmp = Milestones[i];
                   
                    idx = i;
                    nearest = dist;
                    
                }
                //else
                //{
                //   /// tmp = Milestones[0];
                //    src.visited = 1;
                //    idx = i;
                //}
            }
            // tmp.score = pos;
            Milestones[idx].score = pos;
            return Milestones[idx];
        }

        public bool notIn(List<node> list, node val)
        {
            for(int i=0;i<list.Count;i++)
            {
                if (val.GetHashCode() == list[i].GetHashCode())
                {
                    
                    return false;
                }
            }
            return true;
        }
        public void getCenters()
        {
            //Centers=Cluster.Kmedoids(SafeSamples);
           clusterList= Cluster.Kmedoids(Index);
            for(int i=0;i<clusterList.Count;i++)
            {
                Cartesian point = new Cartesian();
                point.x = Map.Xmin + clusterList[i].x * 20; //resol_dist
                point.y = Map.Ymin + clusterList[i].y * 20;
                point.z = Map.Zmin + clusterList[i].z * 20;

                Centers.Add(point);

            }
        }

        void removeUnsafe()
        {
            for(int i=0;i<PRM.Count;i++)
            {
                node curr = PRM[i];
                for(int j=0;j<curr.neighbours.Count;j++)
                {
                    //procedure used is similar to Simple DDA
                    int dx = curr.neighbours[j].index.x - curr.index.x;
                    int dy= curr.neighbours[j].index.y - curr.index.y; 
                    int dz= curr.neighbours[j].index.z - curr.index.z;
                    int l;
                    if (Math.Abs(dx) >= Math.Abs(dy) && Math.Abs(dx) >= Math.Abs(dz))
                        l = Math.Abs(dx);
                    else if (Math.Abs(dy) >= Math.Abs(dz))
                        l = Math.Abs(dy); 
                    else
                        l = Math.Abs(dz);

                    double xinc = dx / l;
                    double yinc = dy / l;
                    double zinc = dz / l;
                    double x = curr.index.x, y = curr.index.y, z = curr.index.z;
                    bool rem = false;
                    while(x!= curr.neighbours[j].index.x && y!= curr.neighbours[j].index.y && z!= curr.neighbours[j].index.z)
                    {
                        x += xinc;
                        y += yinc;
                        z += zinc;
                        if(Map.Probability_map.probMap[(int)x,(int)y,(int)z]==1)  //change threshold to lower value for better performance or incorporate turn radius
                        {
                            //takes care of quarantined free space ie Pseudo free space
                            curr.neighbours.RemoveAt(j);
                            rem = true;
                            break;
                           
                        }
                    }
                    if(rem)
                    j--; //done to avoid skipping of term
                }
            }
        }
        public void displayCenters()
        {
            for(int i=0;i<clusterList.Count;i++)
            {
                //Console.WriteLine("Id is"+i+"\tX is" + clusterList[i].x + "\t Y is" + clusterList[i].y + " \tZ is" + clusterList[i].z);
                Cartesian point = new Cartesian();
                point.x = Map.Xmin + clusterList[i].x * 20;  //resol_dist
                point.y = Map.Ymin + clusterList[i].y * 20;
                point.z = Map.Zmin + clusterList[i].z * 20;

                WayPoint tmp = Map.ECEFtoLLA(point);
                Console.WriteLine("Id is" + i + "\tlat is" + tmp.lat + "\t Lon is" + tmp.lon + " \tAlt is" + tmp.alt);

            }
        }
        public void displayGraph()
        {
            string tmp = "";
            for(int i=0;i<PRM.Count;i++)
            {
                Console.WriteLine("node id is\t" + i);
                
                for (int j=0;j<PRM[i].neighbours.Count;j++)
                {
                    
                    Console.WriteLine("neighbour" + (j + 1) + ":\t" + PRM[i].neighbours[j].index.x + "\t" + PRM[i].neighbours[j].index.y + "\t" + PRM[i].neighbours[j].index.z + "\t");
                    tmp += PRM[i].index.x + "\t" + PRM[i].index.y + "\t" + PRM[i].index.z + "\n";
                    tmp += PRM[i].neighbours[j].index.x + "\t" + PRM[i].neighbours[j].index.y + "\t" + PRM[i].neighbours[j].index.z + "\n\n\n";
                }
               // tmp += "\n\n";
            }
            System.IO.File.WriteAllText(@".\graph.csv", tmp);
        }
        void getSamples()
        {
            while(SafeSamples.Count<=num_sample) //results can be improved by selecting 'n' best samples and removing rest.
            {
                //int i = rnd.Next(0, Map.Probability_map.rows-1);
                //int j= rnd.Next(0, Map.Probability_map.cols-1);
                //int k = rnd.Next(0, Map.Probability_map.height-1);
                int i = rnd.Next(8, 35); // prob had to set manually: otherwise genertates bad samples 
                int j = rnd.Next(0, Map.Probability_map.cols - 1);
                int k = rnd.Next(0, Map.Probability_map.height - 1);
                IntList tmp = new IntList();
                tmp.x = i;
                tmp.y = j;
                tmp.z = k;
                bool repeat = false;
                for(int z=0;z<Index.Count;z++)
                {
                    if (Index[z].x == tmp.x && Index[z].y == tmp.y && Index[z].z == tmp.z)
                        repeat = true;
                }
                if(isSafe(i,j,k) && !repeat)
                {
                    Cartesian point = getNewSample(20, i, j, k);   //resol_dist
                    SafeSamples.Add(point);
                    Index.Add(tmp);
                    //test2
                    WayPoint t = Map.ECEFtoLLA(point);
                    if(t.alt<100)
                    Console.WriteLine(t.lat + " " + t.lon + " " + t.alt);
                    //end test2
                }
            }
            Console.WriteLine("requireed samples found!!");
        }
        bool isSafe(int i, int j,int k)
        {
            if (Map.Probability_map.probMap[i, j, k] < 0.3) //safety param optimum:0.1
                return true;
            else return false;
        }
        Cartesian getNewSample(double resol_dist,int i,int j,int k)
        {
            Cartesian tmp = new Cartesian();
            tmp.x = Map.Xmin + i * resol_dist;
            tmp.y = Map.Ymin + j* resol_dist;
            tmp.z = Map.Zmin + k * resol_dist;

            return tmp;

        }

        void writefile(List<WayPoint> val,string filename)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                file.WriteLine("QGC WPL 110");
                for (int g = 0; g < val.Count; g++)
                {
                    string tmp;
                    tmp = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}", g, "1", "0", "16", "0", "0", "0", "0", val[g].lat, val[g].lon, val[g].alt, "1");
                    file.WriteLine(tmp);
                }
            }
        }
    }

    struct nodeDist
    {
        public node Node;
        public double dist;
    }
    class node  //converted to class because structs cannot be modified once created
    {
        public Cartesian Point;
        public IntList index;
        public double score; // heurestic function h
        public double pcost; // path function g
        public int visited; // expanded or not
        public List<node> neighbours; // K nearest neighbours
        public List<node> successor;  // list of successors
        public node parent { get; set; } // parent node link
        public node(int i)
        {
            Point = new Cartesian();
            index = new IntList();
            score = new double();
            pcost = new double();
            visited = new int();
            neighbours = new List<node>();
            successor = new List<node>();
            
        }
    }

    struct IntList
    {
        public int x { get; set; }
        public int y { get; set; }
        public int z { get; set; }
    }
}
